//ES5
// var firstName1 = 'Cybersoft';
console.log("TOAN");
//cho phep gan lai gia tri
// firstName1 = 'Cybersoft1';

//cho phep khai bao trung ten bien
// var firstName1 = 'Cybersoft2';

// console.log(firstName1);
//ES6
// let firstName2 = 'Cybersoft new';
//cho phep gan lai gia tri
// firstName2 = 'Cybersoft new2';
//khong cho phep khai bao trung ten bien
// let firstName2 = 'Cybersoft new3';
// console.log(firstName2);

//const
// const PI = 3.14;
//Khong duoc phep gan lai gia tri
// PI = 3.1414;
//Khong duoc khai bao trung ten
// const PI = 3.15;
// console.log(PI);


//Hoisting

//ES5 var


//Goi ham va bien
// console.log(sum(2,3));
// console.log(city); //Undefined : chua gan gia tri
//chua duoc khai bao / khoi tao
//Khai bao ham
// function sum(x,y){
//     return x + y;
// }
//Khai bao bien
// var city = "Ho Chi Minh";
// console.log(city);


//ES6 let/const
// console.log(NAME); //initialization
// console.log(address); //initialization
// console.log(city); //undefined
// const NAME = "Cybersoft"; 
// let address = "456 Su van hanh"; 
// var city = "HCM";

//Scope
//global
// let name = "Benjamin Tran";
// function weight(){
    //local
    // let w = 100;
    
    // if(w > 60){
        //local
        // var nickName = "Hypebeast";
        // let nickName = "Hypebeast";
        // console.log(nickName+" nặng: "+w+"kg");
    // }
    // console.log(nickName+" hơi gầy!Ráng ăn thêm");
// }
// weight();
// console.log(name);
// console.log(w);
// console.log(nickName);

//Arrow function
// function tenham(thamso){
    //xu ly
// }

// let hello = function(name){
//     return "Hello " + name;
// }
// console.log(hello("Benjamin"));
//ES6
// let hello1 = (name) =>{
//     return "Hello1 " + name;
// }
// console.log(hello1("Fabo"));

//chi co 1 tham so, co the bo dau tron ()
// let hello1 = name =>{
//     return "Hello1 " + name;
// }
// console.log(hello1("Fabo"));


//Neu chi co 1 lenh return, thi bo dau {} va tu khoa return 
// let hello1 = (name) => "Hello1 " + name;

// console.log(hello1("Fabo"));

//Loi cu phap
// let hello = (name)=>{
//     return "Hello: " + name;
// }
// console.log(hello("Terry"));

// let hello = (name)
// =>{
//     return "Hello: " + name;
// }
// console.log(hello("Terry"));

// let hello = name
// => "Hello: " + name;

// console.log(hello("Terry"));


// let hello = (name)
// =>{
//     return "Hello: " + name;
// }
// console.log(hello("Terry"));

// let hello = (name)=>
// {
//     return "Hello: " + name;
// }
// console.log(hello("Terry"));

// let hello = name =>
//  "Hello: " + name;

// console.log(hello("Terry"));

// let hello = (
//     name
//     )=>{
//     return "Hello: " + name;
// }
// console.log(hello("Terry"));

// console.log(typeof (() => "Hello Benjamin"));
// let hello = () => "Hello Toan";
// console.log(typeof hello);


// This
// ES5
// let hocVien = {
//     hoTen: "Le Bao Paul",
//     lop:"Streetwear",
//     diemThi: [10,9,8],
//     // layThongTinHocVien: function(){
//         //C1: Bien tam:
//         // var _bind = this;
//         //this la hocVien
//         //_bind la hocVien
//         // this.diemThi.map(function(diem,index){
//         // context
//         // console.log("Ho Ten: "+ _bind.hoTen + " -Lop: "+_bind.lop);
//         // console.log("Diem thi "+index+": "+diem)
//         // });
//     // }

//     layThongTinHocVien: function(){
//         // C2: su dung ham bind() de dinh nghia lai ngu canh cua this
//         this.diemThi.map(function(diem,index){
//         // context
//         console.log("Ho Ten: "+ this.hoTen + " -Lop: "+this.lop);
//         console.log("Diem thi "+index+": "+diem)
//         }.bind(this));
//     }
// }

// hocVien.layThongTinHocVien();

//ES6
// let hocVien = {
//     hoTen: "Le Bao Paul",
//     lop:"Streetwear",
//     diemThi: [10,9,8],
//     layThongTinHocVien:()=>{
        
//         this.diemThi.map((diem,index)=>{
//         // context
//         console.log("Ho Ten: "+ this.hoTen + " -Lop: "+this.lop);
//         console.log("Diem thi "+index+": "+diem)
//         });
//     }
// }

// hocVien.layThongTinHocVien();

// hello();
//Khai bao
// function hello(){
//     console.log("Hello");
// }

// hello();

//Khai bao

// let hello = ()=>{
//     console.log("Hello");
// }
// hello();

//ES5


//ES6
// let getUserInfo = (name = "HaoHero",age = "99")=>{
    
    
//    if(age >= 18 && age < 30){
//       console.log(name + " đang còn trẻ. " + name + " nên đi chơi nhiều");
//    }else{
//        console.log(name + " đã " + age + " tuổi.Nên ở nhà đi!");
//    }
// }

// getUserInfo();


//Rest Parameter
// let tinhDTB = (...danhSachDiem)=>{
//     // console.log(danhSachDiem);
//     let tongDiem = 0;
//     for(let i = 0;i < danhSachDiem.length;i++){
//         tongDiem += danhSachDiem[i];
//     }
//     let dtb = tongDiem / danhSachDiem.length;
//     console.log(dtb);
// }

// tinhDTB(9,8,10,7,6,4);

//Spread Operator
//Them phan tu vao mang
// let mangC = [1,2,3,4];
// let mangD = [...mangC];
// mangD.push(5);
// mangD.push(6);
// mangD.push(7);
// console.log(mangD);

//Them thuoc tinh cho doi tuong
// let hypebeast = {
//     name:"Qrew",
//     expert:"Sneaker"
// }

// let newHypebeast = {
//     age:33,
//     ...hypebeast
// }

// console.log(newHypebeast);

//Destructuring

//Array
// let programs = ['JavaScript','Java','Python'];

//ES5
// console.log(programs[0]);
// console.log(programs[1]);
// console.log(programs[2]);

//ES6
// let [first,second,third] = programs;
// console.log(first);
// console.log(second);
// console.log(third);

//Object
// let pet = {
//     name: 'Gau Dan',
//     age: 3,
//     breed: 'Golden',
//     size:{
//         weight: '30kg',
//         height: '56cm'
//     }
// }

//ES5
// var name = pet.name;
// var age = pet.age;
// var size = pet.size;

// console.log(name,age);
// console.log(size);

//ES6
// let {name,age} = pet;
// let {weight,height} = pet.size;
// console.log(name,age);
// console.log(weight,height);

// let {weight: w,height: h} = pet.size;
// console.log(w,h);

//Template String
// let pet = "Cá";
// let action = "bơi";
//Mình là cá, việc của mình là bơi
//ES5(
// console.log("Mình là " + pet + " việc của mình là " + action);

//ES6
// console.log(`Mình là ${pet} ,việc của mình là ${action}`);

// let a = 2;
// let b = 3;
// console.log(`Sum = ${a + b}`);

// document.getElementById('section1').innerHTML = `
//     <div class="class1">
//         <p>
//         Mình là ${pet} ,việc của mình là ${action}
//         </p>
//     </div>

// `;


//Object Literal
//ES5
// var name = "Ben";
// var myObject = {
//     name: name,
//     sayHi: function(){
//         console.log("Hello,my name is "+this.name);
//     }
// }

// myObject.sayHi();

//ES6
// let name = "Ben";
// let myObject = {
//     name,
//     sayHi(){
//         console.log("Hi,my name is "+this.name);
//     }
// }

// myObject.sayHi();

// let name = "ten";
// let myObject = {
//     //ten : "Ben"
//     [name]: "Ben",
//     sayHi(){
//         console.log("Hi,my name is "+this.ten);
//     }
// }

// myObject.sayHi();

//for
//empty element
let currencies = ['VND',,'USD','SGN'];
//index
// for(let i = 0 ; i < currencies.length ; i++){
//     console.log(i,currencies[i]);
// }

//ES5
//for ...in
//index
// for(let i in currencies){
//     console.log(i,currencies[i]);
// }

//ES6
//for ... of
//value
// for(let value of currencies){
//     console.log(value);
// }

// for(let [index,value] of currencies.entries()){
//     console.log(index,value);
// }


//OOP
