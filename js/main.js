//ES5
// console.log(sum(2,3));

//Node.js
// var callSum = require('./sum.js');
// console.log("SUM = "+callSum(5,4));

//ES6
// import callSum from './sum.js';
// console.log("SUM = "+callSum(5,4));

// import {sum as callSum} from './sum.js';
// console.log("SUM = "+callSum(5,4));

// import {sum,sum2} from './sum.js';
// console.log("SUM = "+sum(5,4));
// console.log("SUM = "+sum2(5,4,3));

import * as sumFunctions from './sum.js';
console.log("SUM = "+sumFunctions.sum(5,4));
console.log("SUM = "+sumFunctions.sum2(5,4,3));

