//OOP
//ES5

//class: lop doi tuong
//function: camel getInfo
//pascal camel GetInfo
// function Student(name,address){
//     this.name = name;
//     this.address = address;
// }

// var st = new Student("Ben","New York");
// console.log(st.name,st.address);

//ES6
// class Student{
//     constructor(name,address){
//         this.name = name;
//         this.address = address;
//     }
// }

// let student = new Student("Ben","New York");
// console.log(student.name,student.address);

//Extend
//ES5
// function Father(name){
//     this.name = name;
// }
// Father.prototype.createColorEyes = function(){
//     console.log("Black");
// }

// function Children(name){
    //Ke thua thuoc tinh tu Father
    // Father.apply(this,arguments);
// }
//Ke thua phuong thuc tu Father
// Children.prototype = new Father;
// Children.prototype.createColorEyes = function(){
//     console.log("Brown");
// }

// Children.prototype.createSkinColor = function(){
//     console.log("Brown");
// }

// var child = new Children('Con');
// console.log(child.name);
// child.createColorEyes();
// child.createSkinColor();


//ES6
class Father{
    constructor(name) {
        this.name = name;
    }
    createColorEyes(){
        console.log("Black");
    }
}

//extends
//Ke thua thuoc tinh va phuong thuc tu class Father
class Children extends Father{
    createSkinColor(){
        console.log("Brown");
    }
}

// let child = new Children('Con');
// console.log(child.name);
// child.createColorEyes();
// child.createSkinColor();

//Method Overriding
// class Person{
//     constructor(name) {
//         this.name = name;
//     }
// }
// Person.prototype.getName = function(){
//     return this.name;
// }

// class Student extends Person{
    //super
//     getPersonName(){
//         return super.getName();
//     }
// }
// Student.prototype.getName = function(){
//     return "Hello " + this.name;
// }
// let st = new Student("Ben");
// console.log(st.getName());
// console.log(st.getPersonName());

//Proxy
let pet = {
    name: "cau vang",
    age: 2,
    breed: "shiba"
}

// let pet1 = new Proxy(pet,{
//     get(target,prop,receiver){
//         if(typeof target[prop] === 'string'){
//             return target[prop].toUpperCase();
//         }
//         return target[prop];
//     }
// });

// console.log(pet1.name);
// console.log(pet1.age);
// console.log(pet1.breed);

let pet1 = new Proxy(pet,{
    set(target,prop,value){
        if(prop === 'age' && typeof value !== 'number'){
            throw new TypeError('Age must be a number');
        }
        target[prop] = value;
        return true;
    }
});

// pet1.age = '123';
pet1.age = 10;
console.log(pet1.age);