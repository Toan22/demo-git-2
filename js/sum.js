function sum(a,b){
    return a + b;
}

function sum2(a,b,c){
    return a + b + c;
}
//NodeJS
// module.exports = sum;

//ES6
// export default sum,sum2;

export {sum,sum2};

